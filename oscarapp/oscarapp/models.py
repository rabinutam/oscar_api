'''
Refer:
https://docs.djangoproject.com/en/1.9/ref/models/fields/

Note:
There must not be seperate Member table, use django.contrib.auth.models.User
'''

from django.db import models

class Movie(models.Model):
    title = models.CharField(max_length=100, unique=True)
    year = models.CharField(max_length=4)
    rated = models.CharField(max_length=20)
    director = models.CharField(max_length=100)
    actors = models.CharField(max_length=200)
    plot = models.TextField(null=True)
    poster_url = models.URLField(null=True)

class Voter(models.Model):
    age = models.IntegerField()
    gender = models.CharField(max_length=1)
    zipcode = models.CharField(max_length=5)

class VoteCount(models.Model):
    count = models.IntegerField()
    movie = models.ForeignKey(Movie)
    voter = models.ForeignKey(Voter)
