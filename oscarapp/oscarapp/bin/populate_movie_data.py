'''
!! Warning:
    Use this script for initial setup only, or
    to restore corrupt database
    Using it at other times can create errors in data relationship

Usage:
    There are some issues with django so Usage 1 cannot be applied now. Follow Usage 2.

Usage 1:
    $ cd oscarapp/bin
    $ python populate_init_db_data.py

Usage 2:
    $ python manage.py shell
    >>> from oscarapp.bin.populate_init_db_data import setup_init_db
    >>> setup_init_db()
'''

import django
import os
import sys

# path to BASE_DIR
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
sys.path.append(BASE_DIR)

# oscarapp settings
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "oscarapp.settings")
django.setup()

# do this after settings
from oscarapp import models
from movie_data import movies

def prepare_data():
    moviesx = []
    for movie in movies:
        moviex = {
                'title': movie['Title'],
                'year': movie['Year'],
                'rated': movie['Rated'],
                'director': movie['Director'],
                'actors': movie['Actors'],
                'plot': movie['Plot'],
                'poster_url': movie['Poster'],
                }
        moviesx.append(moviex)
    return moviesx

def setup_init_db():
    movies = prepare_data()
    for movie in movies:
        try:
            models.Movie.objects.get(title=movie['title'])
        except models.Movie.DoesNotExist:
            models.Movie.objects.create(**movie)

if __name__ == '__main__':
    setup_init_db()
